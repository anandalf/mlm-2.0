% Falta verificar se o calculo bate direitinho.

function D = kernelFunction(X, refPoints, kernel)
  switch kernel.name
      case {'linear', 'lin', 'l'}
        D = pdist2(X,refPoints);       
        
      case {'rbf','RBF','gaussian','g','Gaussian'}
        if ~isfield(kernel,'sigma')
            disp('Define value of kernel.sigma.')
            return
        end    
        
        K = gaussian(X, refPoints, kernel); % NumSamples X NumReferencePoints
        
% %         diagX = diag(gaussian(X,X,kernel));
% %         diagR = diag(gaussian(refPoints,refPoints,kernel));
%         diagX = ones(size(X,1),1);          % 1 X NumSamples
%         diagR = ones(1,size(refPoints,1));  % 1 X NumReferencePoints               
%         D = repmat(diagX,1,size(K,2)) + repmat(diagR,size(K,1),1) - 2*K;        
        D = real(sqrt(2*ones(size(K,1),size(K,2)) - 2*K));
        
      case {'quadratic','Quadratic','q'}
        diagX = diag(quadratic(X,X));
        diagR = diag(quadratic(refPoints,refPoints));
        K     = quadratic(X, refPoints);
        
        D = sqrt(repmat(diagX,1,size(K,2)) + repmat(diagR',size(K,1),1) - 2*K);
        
      case {'polynomial','poly','p'}
        if ~isfield(kernel,'polyOrder')
            disp('Define value of kernel.polyOrder.')
            return
        end
        
        diagX = diag(polynomial(X,X,kernel));
        diagR = diag(polynomial(refPoints,refPoints,kernel));
        K     = polynomial(X, refPoints, kernel);
        
        D = sqrt(repmat(diagX,1,size(K,2)) + repmat(diagR',size(K,1),1) - 2*K);        
  end
  
%   X2=sum(refX.^2,2); DX2=repmat(X2,1,n)+repmat(X2',n,1)-2*(refX*refX');
%   DX=sqrt(DX2); 
end

function K = gaussian(X,refPoints,kernel)
    K = exp(-(1/(2*kernel.sigma^2))*(repmat(sqrt(sum(X.^2,2).^2),1,size(refPoints,1))...
        -2*(X*refPoints')+repmat(sqrt(sum(refPoints.^2,2)'.^2),size(X,1),1)));
end

function K = quadratic(X,refPoints)
    dotproduct = (X*refPoints');
    K = dotproduct.*(1 + dotproduct);
end

function K = polynomial(X,refPoints,kernel)
    dotproduct = (X*refPoints');

    K = dotproduct;
    for i = 2:kernel.polyOrder
        K = K.*(1 + dotproduct);
    end
end
