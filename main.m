%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  This code evalutes the performance of MLM with kernel: linear, gaussian,
%  quadratic and polynomial for classification and regression problems.
%
%  INPUTS: 
%  problem  - 'class' or 'reg' which defines if the output result will be a
%             percentage of accuracy [0,1] or RMSE.
%  normalization - 'y' or 'n'.
%  fileName -   name and path (without the .mat extension) defined by the user where the
%               results will be saved.
%  analysis.flag - 0 or 1 to generate table and figures about the results and
%                  the result of the Friedman Statistical test (default
%                  alpha=0.05).
%  analysis.myCaption - string with the caption of your table
%  analysis.titles    - string with the title of your figures
%
%  Author: Ananda Freire (IFCE- - Brazil)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result,param] = main(problem,normalization,fileName,analysis)

    InitEnvironments;
    externalReps = 3;  % Monte Carlo division

    % Takes all datasets' names inside the folder for CLASSIFICATION or
    % REGRESSION
    switch problem
        case 'class' 
           dbIndNames = dir(strcat(dbIndPathClass, '*.mat'));
        case 'reg'
           dbIndNames = dir(strcat(dbIndPathRegre, '*.mat'));
    end
    
    result.lin  = zeros(externalReps,numel(dbIndNames));
    result.gaus = zeros(externalReps,numel(dbIndNames));
    result.quad = zeros(externalReps,numel(dbIndNames));
    result.poly = zeros(externalReps,numel(dbIndNames));      

    param.K             = zeros(externalReps,numel(dbIndNames));
    param.sigma2        = zeros(externalReps,numel(dbIndNames));
    param.polyOrder     = zeros(externalReps,numel(dbIndNames));

    for j=1:numel(dbIndNames) % For each dataset in folder 
        [X,y,data] = loadData(j,dbIndNames,problem);
        for i=1:externalReps
            disp(strcat('  Repetition :',num2str(i)));
            [trainData, testData] = separateData(X,y,data,i,normalization,problem);
                        
            %MLM - kernel linear
            kernel.name     = 'linear';
            param.K(i,j)    = modelSelection(trainData, kernel, 0.1:0.1:1);
            result.lin(i,j) = mlmPerform(trainData, testData, param.K(i,j), kernel, problem);
            
            %MLM - kernel gaussian
            kernel_g.name   = 'rbf';
            [kernel_g,~,param.sigma2(i,j)] = paramSelection(trainData, param.K(i,j), kernel_g, 1:1:35);
            result.gaus(i,j)= mlmPerform(trainData, testData, param.K(i,j), kernel_g, problem);
            
             %MLM - kernel quadratic
            kernel.name     = 'q';
            result.quad(i,j)= mlmPerform(trainData, testData, param.K(i,j), kernel, problem);

            %MLM - kernel polinomial
            kernel_p.name   = 'poly';
            [kernel_p,~,param.polyOrder(i,j)] = paramSelection(trainData, param.K(i,j), kernel_p, 3:1:10);
            result.poly(i,j)= mlmPerform(trainData, testData, param.K(i,j), kernel_p, problem);            
        end
    end
    
    % Saves the results and parameters 
    save(strcat(fileName,'.mat'),'result','param');
    
    % Performs a Friedman test and creates tables and figures that reveal
    % the results
    if analysis.flag
        disp('Creating table and figures....')
        [stat_results,~] = testFriedman(0.05, fileName); %default value for alpha.  
        createTable(fileName, analysis.myCaption, param.K, stat_results);
        createFigures(analysis.titles,fileName);
        disp(' ');
    end
end

function res = mlmPerform(trainData, testData, K, kernel, problem)   
    model = train(trainData, K, kernel);
    yhat = predict(model, testData, kernel, 'nn');
    switch problem
        case 'class' 
           res = MLMUtil.getAccuracy(testData.y, yhat); 
        case 'reg'
            res = MLMUtil.getRMSE(testData.y, yhat);
    end    
end

function [X,y,data] = loadData(j,dbIndNames,problem)
    InitEnvironments;
    switch problem
        case 'class' 
           dbIndPath    = dbIndPathClass;
           dbPath       = dbPathClass;
        case 'reg'
           dbIndPath    = dbIndPathRegre;
           dbPath       = dbPathRegre;
    end 
    
    load(strcat(dbIndPath,dbIndNames(j).name));    
    load(strcat(dbPath,dbIndNames(j).name));
    
    while(any(isnan(X)==1) | any(isinf(X)==1))        
        disp('Nan or Inf values in kernel matrix. Skip to next dataset.');
        dbIndNames(j) = []; %excludes data with problem
        load(strcat(dbIndPath,dbIndNames(j).name));    
        load(strcat(dbPath,dbIndNames(j).name));
    end    
    disp(dbIndNames(j).name);
end

function [trainData, testData] = separateData(X,y,data,i,normalization,problem)
    switch normalization
        case 'y'
            trainData.x = MLMUtil.normalize(X(data.indTrain(i,:),:));
            testData.x  = MLMUtil.normalize(X(data.indTest(i,:),:)); 
        case 'n'
            trainData.x = X(data.indTrain(i,:),:);
            testData.x  = X(data.indTest(i,:),:); 
    end
           
    switch problem
        case 'class'
            trainData.y = MLMUtil.outputEncoding(y(data.indTrain(i,:),:));
            testData.y  = MLMUtil.outputEncoding(y(data.indTest(i,:),:));
        case 'reg'
            switch normalization
                case 'y'
                    trainData.y = MLMUtil.normalize(y(data.indTrain(i,:),:));
                    testData.y  = MLMUtil.normalize(y(data.indTest(i,:),:)); 
                case 'n'
                    trainData.y = y(data.indTrain(i,:),:);
                    testData.y =  y(data.indTest(i,:),:);  
            end
    end   
end

function [results, p] = testFriedman(alpha, fileName)
    import utils.*
    
    load(strcat(fileName,'.mat'))
    
    InitEnvironments;
    dbIndNames = dir(strcat(dbIndPathRegre, '*.mat'));
    
    p = zeros(numel(dbIndNames),1);
    results = zeros(numel(dbIndNames),3);
    
    for i=1:numel(dbIndNames)
        x = [result.lin(:,i) result.gaus(:,i) result.quad(:,i) result.poly(:,i)];
        
        [p(i),~,stats] = friedman(x,1,'off');
        
        if p(i)>alpha
            results(i,:) = ones(1,3); % all are equivalent
        else
            % if 1, means that this kernel is equivalent to the linear kernel.
            % 0 otherwise
            c = multcompare(stats,alpha,'off','dunn-sidak');            
            results(i,:) = (c(1:3,end)'>alpha); 
        end
    end      
    close all;  %close figures that appears.
end

function createTable(fileName, myCaption, K, stat_results)

% HEADER NEEDED
%     \documentclass{article}
%     \usepackage{amssymb}% http://ctan.org/pkg/amssymb
%     \usepackage{pifont}% http://ctan.org/pkg/pifont
%     \usepackage{booktabs}
%     \usepackage{multirow} 
%     \newcommand{\cmark}{\ding{51}}%
%     \newcommand{\xmark}{\ding{55}}%

    load(strcat(fileName,'.mat'))
    
    InitEnvironments;
    dbIndNames = dir(strcat(dbIndPathRegre, '*.mat'));
    
    sig    = param.sigma2;
    pOrder = param.polyOrder;
    
    fileID = fopen(strcat(fileName,'.tex'),'w');
    
    fprintf(fileID,'%20s\n','\begin{table}[ht!]');
    fprintf(fileID,'%20s\n','\centering');
    fprintf(fileID,'%100s\n',strcat('\caption{',myCaption,'}'));
    fprintf(fileID,'%20s\n','\label{tab:results}');
    fprintf(fileID,'%30s\n','\begin{tabular}{llccccc}');
    fprintf(fileID,'%20s\n','\toprule[1.5pt]');
    fprintf(fileID,'%20s\n','\midrule');
    
    fprintf(fileID,'%100s\n','Datasets & $K$ &  & Linear  & Gaussian  & Quadratic  & Polinomial\\');
    fprintf(fileID,'%20s\n','\midrule');
    
    for i=1:numel(dbIndNames)
        x       = [result.lin(:,i) result.gaus(:,i) result.quad(:,i) result.poly(:,i)];
        mx      = mean(x);
        stdx    = std(x);
        ms      = mean(sig(:,i));
        stds    = std(sig(:,i));
        mp      = mean(pOrder(:,i));
        stdp    = std(pOrder(:,i));
        mk      = mean(K(:,i));
        stdk    = std(K(:,i)); 
        
        fprintf(fileID,'%80s ',strcat('\multirow{3}{*}{',dbIndNames(i).name),'} ');
        fprintf(fileID,'%19s %3.2f %5s %3.2f %14s','& \multirow{3}{*}{',mk,'$\pm$',stdk,'} & rmse ');
        
        %rmse
        for j=1:4
            fprintf(fileID,'%2s %3.2f %5s %3.2f ','& ',mx(j),'$\pm$',stdx(j));
        end        
        fprintf(fileID,'%2s \n','\\');
        
        %parameters
        fprintf(fileID,'%70s ',' &  & parameters ');       
        fprintf(fileID,'%6s %3.2f %5s %3.2f %6s %3.2f %5s %3.2f %2s \n','& - &',ms,'$\pm$',stds,'& - &',mp,'$\pm$',stdp,'\\');              
        
        %statistical validation
        fprintf(fileID,'%70s ',' &  &  & -');       
        for j=1:3
            if stat_results(i,j) == 0
               fprintf(fileID,'%8s ','& \xmark');  % x -> not equivalents
            else
               fprintf(fileID,'%8s ','& \cmark');  % ok -> equivalents
            end            
        end
        fprintf(fileID,'%2s \n','\\');
        fprintf(fileID,'%20s\n','\midrule');
    end    
    
    fprintf(fileID,'%20s\n','\bottomrule[1.5pt]');
    fprintf(fileID,'%20s\n','\end{tabular}');
    fprintf(fileID,'%20s\n','\end{table}');
    fclose(fileID);   
end

function createFigures(titles, fileName)
    import utils.*
    load(strcat(fileName,'.mat'))
    
    InitEnvironments;
    dbIndNames = dir(strcat(dbIndPathRegre, '*.mat'));
    num = numel(dbIndNames);
    
    r  = result.lin;
    rg = result.gaus;
    rp = result.poly;
    rq = result.quad;   
    
    cmap = [1 102/255 102/255; 102/255 1 178/255; 102/255 178/255 1; 1 1 102/255];

    if mod(num,3) == 0
        k = 3;
    elseif mod(num,4) == 0
        k = 4;
    elseif mod(num,5) == 0
        k = 5;
    else
        k=2;
    end
    
    j=1;
    if (num/k)<1
        figure;
        title(titles); 
        h1 = {r(:,j); rg(:,j); rq(:,j); rp(:,j)};
        aboxplot_meu(h1,cmap,'labels',{dbIndNames(j).name}); % Advanced box plot
        legend({'Linear','Gaussian','Quadratic','Polynomial'});
        grid on
        saveas(gcf,strcat(fileName,'.png'));
        saveas(gcf,strcat(fileName,'.fig'));
    else
        for i=1:(num/k)        
            figure;
            title(titles); 
            h1 = {r(:,j:(j+k-1)); rg(:,j:(j+k-1)); rq(:,j:(j+k-1)); rp(:,j:(j+k-1))};
            aboxplot_meu(h1,cmap,'labels',{dbIndNames(j:(j+k-1)).name}); % Advanced box plot
            legend({'Linear','Gaussian','Quadratic','Polynomial'});
            grid on
            j = j+k;
            saveas(gcf,strcat(fileName,'_',num2str(j),'.png'));
            saveas(gcf,strcat(fileName,'_',num2str(j),'.fig'));
        end
    end
    
end