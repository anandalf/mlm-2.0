function [kernel, error, finalParam] = paramSelection(data, K, kernel, param, nFolds, method, lambda)

    if (nargin < 5)
        nFolds = 10;
    end
    if (nargin <= 6)
        method = 'random';
    end
    if (nargin < 7)
        lambda = 0;
    end

    N = size(data.x, 1);
    if (nFolds == N)
        CVO = cvpartition(N, 'Leaveout');
    else
        CVO = cvpartition(N, 'k', nFolds);
    end
    amseValues = zeros(N, length(param));

    for i = 1: nFolds
        learnPoints.x = data.x(training(CVO, i), :);
        learnPoints.y = data.y(training(CVO, i), :);
        testData.x = data.x(test(CVO, i), :);
        testData.y = data.y(test(CVO, i), :);

        for j = 1: length(param)
            switch kernel.name
                case {'rbf','RBF','gaussian','g','Gaussian'}
                    kernel.sigma = param(j);
                case {'polynomial','poly','p'}
                    kernel.polyOrder = param(j);
            end
            [model] = train(learnPoints, K, kernel, method, lambda); 
%             yhat = predict(model, testData, kernel, method);
%             errors = testData.y - yhat;
%             amseValues(i, j) = mean(mean(errors.^2));
            [amseValues(i, j)] = AMSE(testData, model, kernel);
        end
    end

    Ecv = mean(amseValues);
    [error, ind] = min(Ecv);
    
    finalParam = param(ind);
    switch kernel.name
        case {'rbf','RBF','gaussian','g','Gaussian'}
            kernel.sigma = param(ind);
        case {'polynomial','poly','p'}
            kernel.polyOrder = param(ind);
    end
end

function [amse] =  AMSE(data, model, kernel)
    DX = kernelFunction(data.x, model.refPoints.x, kernel);
    DYh = DX*model.B;
    DY = pdist2(data.y, model.refPoints.y);
    errors = (DY - DYh);
    amse = mean(mean(errors.^2));
end