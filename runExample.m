clear all
close all
clc

% For a classification problem
problem             = 'class';
normalization       = 'n';
fileName            = 'class_results';
analysis.flag       = 1;
analysis.myCaption  = 'Results for classification problems';
analysis.titles     = 'MLM with different kernels for classification problems';

[result_c,param_c]  = main(problem,normalization,fileName,analysis);

% For a regression problem
problem             = 'reg';
normalization       = 'n';
fileName            = 'regression_results';
analysis.flag       = 1;
analysis.myCaption  = 'Results for regression problems';
analysis.titles     = 'MLM with different kernels for regression problems';

[result_r,param_r]  = main(problem,normalization,fileName,analysis);