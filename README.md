# README #

This is a Matlab's implementation of Minimal Learning Machine that allows the use of linear kernel 
and 3 nonlinear ones (Guassian, quadratic and polynomial).

This code was based on the work of Alencar (2017) available in <goo.gl/Hth2j0>

	Alencar, A. "Máquina De Aprendizagem Mínima: Aspectos Teóricos E Práticos". Dissertation. 
	Federal University of Ceará, 2017.

The resulting code is product of the work:

	Freire, A. and de Sousa Júnior, A. (2017). "On the use of kernel functions in Minimal Learning Machines".
	In: Proceedings of XIII Brazilian Congress on Computational Intelligence (CBIC 2017). Niterói, Rio de 
	Janeiro, Brazil. 30 October to 1 November 2017.


### What is this repository for? ###

* Version 2.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

This library is used to compare the perfomance of MLM with linear kernel and 3 nonlinear ones (Guassian, 
quadratic and polynomial) to classification and regression problems.

### How do I get set up? ###

* Summary of set up
* Configuration

Before using the main.m script, set these variables:

problem             = 'class' or 'reg';
normalization       = 'n' or 'y';					% normalization to a Guassian distibution of mean 0 e std 1
fileName            = '/myFolfer/results_file';  	% do not add the file extension
analysis.flag       = 1 or 0;						% if it should generate tables and figures of the resulting analysis.	
analysis.myCaption  = 'Results for classification problems';  					% LaTex tables caption
analysis.titles     = 'MLM with different kernels for classification problems'; % figures titles

* Dependencies

Needs at least Matlab R2014a with statistics toolbox.

* Database configuration

There are 2 folders: DATA and DATADivided. The first contains the data sets divided by classification or regression problems. 
This set comprehends variable X (inputs) and y (outputs).

The folder DATADivided has a structure named "data" which contains shuffled indexes of the samples for training and test for 30 
independent runs. Plus, there is a another structure named "kfold" which contains, for those same 30 runs, 10-fold cross validation
data partitions.

* How to run tests
You may run the script runExample.m to see how to use the main.m code.

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

anandalf+mlm2@gmail.com


