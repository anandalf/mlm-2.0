%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file initialize the paths for every enviroment needed
%
% July 15th, 2017
% Author: Ananda Freire (IFCE-Brazil)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

     
% Sets of the partitions indexes (Monte Carlo and KFold)
dbIndPathClass = 'DATADivided/CLASSIFICATION/';
dbIndPathRegre = 'DATADivided/REGRESSION/';

% Datasets
dbPathClass = 'DATA/CLASSIFICATION/';
dbPathRegre = 'DATA/REGRESSION/';


