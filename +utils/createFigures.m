clear all
close all
clc


load('accuracy_class.mat');

dbPathClass = 'DATA/CLASSIFICATION/';
dbPathRegre = 'DATA/REGRESSION/';
dbIndNames  = dir(strcat(dbPathClass, '*.mat'));

%% Creating table
Mean = [mean(accuracy)' mean(accuracy_g)' mean(accuracy_q)' mean(accuracy_p)'];
Std  = [std(accuracy)' std(accuracy_g)' std(accuracy_q)' std(accuracy_p)'];
Median = [median(accuracy)' median(accuracy_g)' median(accuracy_q)' median(accuracy_p)'];
Maximum = [max(accuracy)' max(accuracy_g)' max(accuracy_q)' max(accuracy_p)'];
Minimum = [min(accuracy)' min(accuracy_g)' min(accuracy_q)' min(accuracy_p)'];

%% Boxplot
import utils.*

% cmap = [153/255 1 153/255; 153/255 1 1; 153/255 204/255 1; 204/255 153/255 1];
cmap = [1 102/255 102/255; 102/255 1 178/255; 102/255 178/255 1; 1 1 102/255];

title('MLM results with different kernels (Classification)')
h1 = {accuracy(:,1:5); accuracy_g(:,1:5); accuracy_q(:,1:5); accuracy_p(:,1:5)};
aboxplot_meu(h1,cmap,'labels',{dbIndNames(1:5).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

figure;
title('MLM results with different kernels (Classification)')
h2 = {accuracy(:,6:10); accuracy_g(:,6:10); accuracy_q(:,6:10); accuracy_p(:,6:10)};
aboxplot_meu(h2,cmap,'labels',{dbIndNames(6:10).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

figure;
title('MLM results with different kernels (Classification)')
h3 = {accuracy(:,11:16); accuracy_g(:,11:16); accuracy_q(:,11:16); accuracy_p(:,11:16)};
aboxplot_meu(h3,cmap,'labels',{dbIndNames(11:16).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on


%%
load('accuracy_regre.mat');

cmap = [1 102/255 102/255; 102/255 1 178/255; 102/255 178/255 1; 1 1 102/255];

figure;
title('MLM results with different kernels (Regression 1)')
h1 = {accuracy(:,1); accuracy_g(:,1); accuracy_q(:,1); accuracy_p(:,1)};
aboxplot_meu(h1,cmap,'labels',{dbIndNames(1).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

figure;
title('MLM results with different kernels (Regression 1)')
h1 = {accuracy(:,2); accuracy_g(:,2); accuracy_q(:,2); accuracy_p(:,2)};
aboxplot_meu(h1,cmap,'labels',{dbIndNames(2).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

figure;
title('MLM results with different kernels (Regression 1)')
h1 = {accuracy(:,3); accuracy_g(:,3); accuracy_q(:,3); accuracy_p(:,3)};
aboxplot_meu(h1,cmap,'labels',{dbIndNames(3).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

figure;
title('MLM results with different kernels (Regression 1)')
h1 = {accuracy(:,4); accuracy_g(:,4); accuracy_q(:,4); accuracy_p(:,4)};
aboxplot_meu(h1,cmap,'labels',{dbIndNames(4).name}); % Advanced box plot
legend({'linear','gaussian','quadratic','polynomial'});
grid on

% %%
% load('accuracy_regre2.mat');
% 
% cmap = [1 102/255 102/255; 102/255 1 178/255; 102/255 178/255 1; 1 1 102/255];
% 
% figure;
% title('MLM results with different kernels (Regression 2)')
% h1 = {accuracy(:,1:2); accuracy_g(:,1:2); accuracy_q(:,1:2); accuracy_p(:,1:2)};
% aboxplot_meu(h1,cmap,'labels',{dbIndNames(1:2).name}); % Advanced box plot
% legend({'linear','gaussian','quadratic','polynomial'});
% grid on
% 
% figure;
% title('MLM results with different kernels (Regression 2)')
% h1 = {accuracy(:,3); accuracy_g(:,3); accuracy_q(:,3); accuracy_p(:,3)};
% aboxplot_meu(h1,cmap,'labels',{dbIndNames(3).name}); % Advanced box plot
% legend({'linear','gaussian','quadratic','polynomial'});
% grid on
% 
% figure;
% title('MLM results with different kernels (Regression 2)')
% h1 = {accuracy(:,4); accuracy_g(:,4); accuracy_q(:,4); accuracy_p(:,4)};
% aboxplot_meu(h1,cmap,'labels',{dbIndNames(4).name}); % Advanced box plot
% legend({'linear','gaussian','quadratic','polynomial'});
% grid on