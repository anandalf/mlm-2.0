%%%%%%%%%%%%%%%%%%%%%
%
%   Divides the datasets available for Monte Carlo and K-Fold
%   crossvalidation tests. 
%   It's important to highlight that we save only the indexes of the
%   dataset. 
%
%   July 7th, 2017
%   Author: Ananda Freire (IFCE/Brazil)
%%%%%%%%%%%%%%%%%%%%%%%


function data = dataDivision()

dbPathClass = 'DATA/CLASSIFICATION/';
dbPathRegre = 'DATA/REGRESSION/';

containmentClass = 'DATADivided/CLASSIFICATION/';
containmentRegre = 'DATADivided/REGRESSION/';


%% 
externalReps  = 30;  % Monte Carlo division
percentageTest = 0.2; % Percentage of samples that goes to Test pool

internalFolds  = 10;  % Crossvalidation division for each Monte Carlo partition



%%
% Takes all datasets names inside the folder for Classification
dbNames = dir(strcat(dbPathClass, '*.mat'));

for i=1:numel(dbNames) % for each dataset in folder   
    disp(dbNames(i).name);
    dataset   = load(strcat(dbPathClass, dbNames(i).name));
    
    % Monte Carlo kind of data separation
    data = monteCarloDivision4Class(dataset, externalReps, percentageTest);
    % From each training set, divide them in a K-Fold set
    data = kFold4MC(internalFolds,data);  
    
    save(strcat(containmentClass,dbNames(i).name),'data');
end

%%
% Takes all datasets names inside the folder for Regression
dbNames = dir(strcat(dbPathRegre, '*.mat'));

for i=1:numel(dbNames) % for each dataset in folder   
    
    dataset   = load(strcat(dbPathRegre, dbNames(i).name));    
    % Monte Carlo kind of data separation
    data = monteCarloDivision(dataset, externalReps, percentageTest);
    % From each training set, divide them in a K-Fold set
    data = kFold4MC(internalFolds,data);  
    
    save(strcat(containmentRegre,dbNames(i).name),'data');
end

end

function data = monteCarloDivision(dataset, n, percentageTest)
    rows = size(dataset.X,1);
    
    numRowsTest  = round(rows*percentageTest);
    numRowsTrain = rows - numRowsTest;
    
    train = zeros(n,numRowsTrain);
    test = zeros(n,numRowsTest);
    for j=1:n % for each repetition
        indexes = randperm(rows);        
        train(j,:) = indexes(1:numRowsTrain);
        test(j,:)  = indexes(numRowsTrain+1:end);
    end
    data.indTrain = train;
    data.indTest  = test;
end

function data = monteCarloDivision4Class(dataset, n, percentageTest)
    rows    = size(dataset.X,1);
    nlabels = length(unique(dataset.y));
    
    numRowsTest  = round(rows*percentageTest);
    numRowsTrain = rows - numRowsTest;
    
    train = zeros(n,numRowsTrain);
    test = zeros(n,numRowsTest);
     
    for j=1:n % for each repetition
        flag = 0;
        while(~flag)
            indexes = randperm(rows);        
            train(j,:) = indexes(1:numRowsTrain);
            test(j,:)  = indexes(numRowsTrain+1:end);
            
            %indicates if there is at least one representant of each class in training and test data
            labelsTrain = length(unique(dataset.y(train(j,:),:)));
            labelsTest  = length(unique(dataset.y(test(j,:),:)));
            if(labelsTrain == labelsTest && labelsTest == nlabels)
                flag = 1;
            end
        end
    end
    data.indTrain = train;
    data.indTest  = test;
end

function data = kFold4MC(k,data)
    for i=1: size(data.indTrain,1) % for each external division
       data.kfold{i} =  KFold(k, data.indTrain(i,:));
    end
end

function dataKFold = KFold(k,in)
    num_samples = length(in);
    indices = crossvalind('Kfold', num_samples, k);

    ind = randperm(num_samples);
    in = in(ind);

    in_val      = cell(k,1);
    ind_val     = cell(k,1);

    in_train    = repmat({in},k,1);

    % choosing the each validation set with no repetitions
    for j=1:num_samples   
        in_val{indices(j)}(size(in_val{indices(j)},1)+1,:) = in(j);
        ind_val{indices(j)}(size(ind_val{indices(j)},1)+1,:) = j;
    end

    % excluding the validation samples from training set
    for i=1:k
        in_train{i}(ind_val{i}) = [];
    end
    
    dataKFold.indTrain = in_train;
    dataKFold.indVal   = in_val;
end
